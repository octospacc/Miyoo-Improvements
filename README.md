# Miyoo-Improvements

Collection of personal resources created to improve my (or your) Miyoo user experience.

## General Requirements

Resources in this repo assume that:

* You're running the latest [Miyoo CFW](https://github.com/TriForceX/MiyooCFW) release on your Miyoo device;
* You installed RetroArch and [set it up](https://github.com/TriForceX/MiyooCFW/wiki/RetroArch-Setup) optimally;
* You have access to a Linux PC for the parts that require a PC.

### Devices

I only have a PocketGo to test my resources, but all other Miyoo ones (New BittBoy, PowKiddy Q20/Q90/V90) should work for everything (unless otherwise specified), only issue is buttons may not line up.

## Contents

Folders named `rootfs` or `main` contain content that is to be copied directly into the respective SD card partitions.

## Improvements

* Automatically boot the console to RetroArch, booting the last used game (idea and history code from <https://github.com/jahed/powkiddy-v90>):
	* Keeping SELECT pressed will boot RetroArch to the main menu;
	* Pressing START or X at the black screen after the logo will boot into GMenu2X;
	* Pressing B at the black screen will skip the 1-second wait and boot into RetroArch.

## Known issues

* It's not possible to just keep one of the boot select button pressed since starting the system, they have to be pressed at the right time (or spammed).
