#include <SDL/SDL.h>
#include <signal.h>
#include <stdio.h>

/*=============Config=============*/
#define Wait 1200
// PocketGo Keys
#define KeyAlt	SDLK_RETURN	// START
#define KeyAlt2 SDLK_LSHIFT	// X
#define KeyDef	SDLK_LCTRL	// B
/*================================*/

SDL_Surface *Screen = NULL;
SDL_Event Event;
Uint8 *Keys;

void Quit(int c) {
	SDL_Quit();
	exit(c);
}

void PrintQuit(char *Str) {
	printf(Str);
	Quit(0);
}

int main(int argc, char *argv[]) {
	signal(SIGINT, &Quit);
	signal(SIGSEGV, &Quit);
	signal(SIGTERM, &Quit);

	SDL_Init(SDL_INIT_VIDEO);
	Screen = SDL_SetVideoMode(320, 240, 16, SDL_SWSURFACE);

	for (int i = 0; i < 10; i++) {
		while (SDL_PollEvent(&Event)) {
			if (Event.type == SDL_KEYDOWN) {
				SDLKey Key = Event.key.keysym.sym;
				if (Key == KeyAlt || Key == KeyAlt2) {
					PrintQuit("2");
				}
				else if (Key == KeyDef) {
					PrintQuit("1");
				}
			}
		}

		Keys = SDL_GetKeyState(NULL);
		if (Keys[KeyAlt] || Keys[KeyAlt2]) {
			PrintQuit("2");
		}
		else if (Keys[KeyDef]) {
			PrintQuit("1");
		}

		SDL_Delay(Wait/10);
	}

	PrintQuit("1");
	return 0;
}
